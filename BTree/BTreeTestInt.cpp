/**********************************************
* File: BTreeTestInt.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the main driver function for
* the BTree Test for the integer template 
**********************************************/
#include "BTree.h"

/********************************************
* Function Name  : insertAndPrint
* Pre-conditions : BTree<int> *t, int value
* Post-conditions: none
* 
* Takes a pointer to a BTree and a value,
* insertes the value into the BTree, and then
* then prints the results 
********************************************/
void insertAndPrint(BTree<int> *t, int value){
	
	t->insert(value); 
    std::cout << "Traversal of tree after inserting " << value << ": \n"; 
    t->traverse(); 
    std::cout << std::endl; 
}

void deleteAndPrint(BTree<int> *t, int value){
	
	t->remove(value); 
    std::cout << "Traversal of tree after deleting " << value << ": \n"; 
    t->traverse(); 
    std::cout << std::endl; 
}

// Driver program to test above functions 
int main(int argc, char** argv) 
{ 
    // A B-Tree with minium degree 2
	// Keys = 2*2-1 = 3
	// Nodes = 2*2 = 4
	BTree<int> p4_2(2); 

	// 50, 60, 70, 40, 30, 20, 10, 35, 80. Then, delete 60.
	insertAndPrint(&p4_2, 50); 
	insertAndPrint(&p4_2, 60);
	insertAndPrint(&p4_2, 70);
	insertAndPrint(&p4_2, 40);
	insertAndPrint(&p4_2, 30);
	insertAndPrint(&p4_2, 20);
	insertAndPrint(&p4_2, 35);
	insertAndPrint(&p4_2, 80);
	deleteAndPrint(&p4_2, 60);
	
    return 0; 
} 
